FROM java:8
EXPOSE 8082
ADD target/userservice.jar userservice.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","userservice.jar"]
