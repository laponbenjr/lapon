package io.getarrays.userservice.repository;

import java.util.List;

import io.getarrays.userservice.model.RegisterModel;

public interface LoginRepository {

	public RegisterModel verify(RegisterModel input) throws Exception;

	public RegisterModel verifyByProcedure(RegisterModel input) throws Exception;

	public RegisterModel getDataByView(RegisterModel input) throws Exception;

	public RegisterModel getDataByView(Long id) throws Exception;

	public RegisterModel getDataById(Long id) throws Exception;

	public List<RegisterModel> getList(Long id) throws Exception;

}
