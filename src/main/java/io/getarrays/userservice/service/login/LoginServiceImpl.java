package io.getarrays.userservice.service.login;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.getarrays.userservice.model.RegisterModel;
import io.getarrays.userservice.repository.LoginRepository;

@Service
public class LoginServiceImpl implements LoginService {
	private final Logger logger = LogManager.getLogger(LoginServiceImpl.class);
	@Autowired
	LoginRepository loginRepository;

	@Override
	@Transactional(readOnly = true)
	public RegisterModel verify(RegisterModel input) throws Exception {
		logger.info("start verify account");
		try {
			return loginRepository.verify(input);

		} catch (EmptyResultDataAccessException e) {
			return null;
		} catch (DataAccessException e) {
			logger.error(" error dataAcess", e);
			throw new RuntimeException(e.getMessage());
		} catch (Exception e) {
			logger.error(" error ", e);
			throw new RuntimeException(e.getMessage());
		}

	}

	@Override
	@Transactional(readOnly = true)
	public RegisterModel getDataByView(RegisterModel input) throws Exception {
		logger.info("start getDataByView");
		try {
			return loginRepository.getDataByView(input);

		} catch (EmptyResultDataAccessException e) {
			return null;
		} catch (DataAccessException e) {
			logger.error(" error dataAcess", e);
			throw new RuntimeException(e.getMessage());
		} catch (Exception e) {
			logger.error(" error ", e);
			logger.error(" error ", e);
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	@Transactional(readOnly = true)
	public RegisterModel getDataByView(Long id) throws Exception {
		logger.info("start getDataByView");
		try {
			return loginRepository.getDataById(id);

		} catch (EmptyResultDataAccessException e) {
			return new RegisterModel();
		} catch (DataAccessException e) {
			logger.error(" error dataAcess", e);
			throw new RuntimeException(e.getMessage());
		} catch (Exception e) {
			logger.error(" error ", e);
			logger.error(" error ", e);
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public List<RegisterModel> getDataList(Long id) throws Exception {
		logger.info("start getDataByView");
		try {
			return loginRepository.getList(id);

		} catch (EmptyResultDataAccessException e) {
			throw new Exception(e.getMessage());
		} catch (DataAccessException e) {
			logger.error(" error dataAcess", e);
			throw new RuntimeException(e.getMessage());
		} catch (Exception e) {
			logger.error(" error ", e);
			logger.error(" error ", e);
			throw new RuntimeException(e.getMessage());
		}
	}
}
