package io.getarrays.userservice.service.login;

import java.util.List;

import io.getarrays.userservice.model.RegisterModel;

public interface LoginService {

	public RegisterModel verify(RegisterModel input) throws Exception;

	public RegisterModel getDataByView(Long id) throws Exception;

	public RegisterModel getDataByView(RegisterModel input) throws Exception;

	public List<RegisterModel> getDataList(Long id) throws Exception;

}
