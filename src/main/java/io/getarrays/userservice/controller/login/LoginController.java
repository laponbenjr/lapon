package io.getarrays.userservice.controller.login;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.html.parser.Entity;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.getarrays.userservice.core.controller.ResponseController;
import io.getarrays.userservice.model.RegisterModel;
import io.getarrays.userservice.service.login.LoginService;

@RestController
@RequestMapping("/login")
public class LoginController extends ResponseController {

	@Autowired
	LoginService loginService;

//	@PostMapping("/verify")
//	public ResponseVo<RegisterModel> verify(HttpServletRequest request, HttpServletResponse response,
//			@RequestBody RegisterModel input) throws Exception {
//		RegisterModel RegisModel = new RegisterModel();
//		ResponseVo<RegisterModel> responseModel = new ResponseVo<RegisterModel>();
//		RegisModel = loginService.verify(input);
//		if (RegisModel != null) {
//			responseModel.setHeader(initHeaderSuccess(RegisModel));
//			responseModel.setData(RegisModel);
//		}
//		return responseModel;
//	}

	@PostMapping("/verify")
	public ResponseEntity<RegisterModel> verify(HttpServletRequest request, HttpServletResponse response,
			@RequestBody RegisterModel input) throws Exception {
		RegisterModel RegisModel = new RegisterModel();
		RegisModel = loginService.verify(input);
		if (RegisModel != null) {
			return ResponseEntity.ok().header("ok", "success").body(RegisModel);
		} else {
			return ResponseEntity.badRequest().header("failed", "Data is null").body(RegisModel);
		}
	}

	@PostMapping("/TestController1")
	public RegisterModel test(HttpServletRequest request, HttpServletResponse response,
			@RequestBody RegisterModel input) throws Exception {
		RegisterModel RegisModel = new RegisterModel();
		RegisModel = loginService.getDataByView(input);

		return RegisModel;
	}

	@GetMapping("/get/{id}")
	public ResponseEntity<RegisterModel> getRegister(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		RegisterModel registerModel = new RegisterModel();
		registerModel = loginService.getDataByView(id);
		if (registerModel != null) {
			return ResponseEntity.ok().header("ok", "success").body(registerModel);
		} else {
			return ResponseEntity.badRequest().header("failed", "Data is null").body(registerModel);
		}
	}
	
	@GetMapping("/getList/{id}")
	public ResponseEntity<List<RegisterModel>> getList(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		List<RegisterModel> registerModel = new ArrayList<RegisterModel>();
		registerModel = loginService.getDataList(id);
		
		if (registerModel != null) {
			//return ResponseEntity.ok().header("ok", "success").body(registerModel);
			return new ResponseEntity<List<RegisterModel>>(registerModel, HttpStatus.OK);
		} else {
			return new ResponseEntity<List<RegisterModel>>(registerModel, HttpStatus.OK);
		}
	}

}
