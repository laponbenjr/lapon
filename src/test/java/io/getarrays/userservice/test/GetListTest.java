package io.getarrays.userservice.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.getarrays.userservice.UserserviceApplication;
import io.getarrays.userservice.model.RegisterModel;
import io.getarrays.userservice.service.login.LoginService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserserviceApplication.class)
@AutoConfigureMockMvc
class GetListTest {
	@Autowired
	private MockMvc mvc;
	
	@Test
	void testGetList() throws Exception {

		String uri = "/login/getList/1";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		ObjectMapper mapper = new ObjectMapper();
		List<RegisterModel> register =  mapper.readValue(content, new TypeReference<List<RegisterModel>>(){});
		System.out.println(register);
//		List<RegisterModel> register = null;
//		register = super.mapFromJson(content,List<RegisterModel>().get);
//		assertTrue(register.size()>0);

	}

	@Test
	void Test_Verify_Success() throws Exception {
		RegisterModel val = new RegisterModel();
		val.setUsername("aha");
		val.setPassword("1234");
		String uri = "/login/verify";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(convertObjectToJsonBytes(val))).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);

		String content = mvcResult.getResponse().getContentAsString();
		ObjectMapper mapper = new ObjectMapper();
		RegisterModel mol = mapper.readValue(content, RegisterModel.class);
		//assertThat(mol,sameBeanAs(val));
		Long id = 1L;
		LoginService loginService = mock(LoginService.class);
		verify(loginService).verify(val);
		assertEquals(mol.getUsername(),val.getUsername());

	}
	
	@Test
	void Test_Verify_Failed() throws Exception {
		RegisterModel val = new RegisterModel();
		val.setUsername("aha");
		val.setPassword("12345");
		String uri = "/login/verify";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(convertObjectToJsonBytes(val))).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(400, status);


	}

	public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsBytes(object);
	}

}
